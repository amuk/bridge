package com.example.sasinghi.blanktry;

/**
 * Created by sasinghi on 4/10/2015.
 */
public class ServicesPOJO {
    private String serviceName;
    private String serviceCost;

    public ServicesPOJO(String serviceName, String serviceCost) {
        this.serviceName = serviceName;
        this.serviceCost = serviceCost;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
