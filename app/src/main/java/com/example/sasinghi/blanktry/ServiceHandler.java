package com.example.sasinghi.blanktry;

/**
 * Created by sasinghi on 3/29/2015.
 */
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceHandler {

    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    public final static String urlPrefix="http://10.142.56.143:8080/IHack/rest/IHackService/";

    public ServiceHandler() {

    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
                                  JSONObject params) {
        try {
            // http client
            url = urlPrefix+url;
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Content-Type", "application/json");
                // adding post params
                if (params != null) {
                    System.out.println("params are...."+params);
                    StringEntity se = null;
                    try {
                        //System.out.println(params.get(0).toString());

                        System.out.println(params.toString());
                        se = new StringEntity(params.toString());
                        se.setContentType("application/json");

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    //System.out.println(se.toString());
                    //httpPost.setEntity(new UrlEncodedFormEntity(params));
                    httpPost.setEntity(se);
                }
                //System.out.println(httpPost.getEntity().toString());
                System.out.println(httpPost.getURI());
                httpResponse = httpClient.execute(httpPost);
                System.out.println(httpResponse.getStatusLine().getStatusCode());


            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    //String paramString = URLEncodedUtils.format(params, "utf-8");
                    String paramString = params.toString();
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);

                httpResponse = httpClient.execute(httpGet);

            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }
}
