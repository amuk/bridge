package com.example.sasinghi.blanktry;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by sasinghi on 3/29/2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<searchResultPOJO> searchResultData;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder  {

        // each data item is just a string in this case
        public TextView card_biz_name;
        public TextView card_distance;
        public TextView card_contact;
        public TextView card_show_on_map;
        public ViewHolder(View v) {
            super(v);
            card_biz_name = (TextView) v.findViewById(R.id.card_text_name);
            card_distance = (TextView) v.findViewById(R.id.card_text_distance);
            card_contact = (TextView) v.findViewById(R.id.card_text_contact);
            card_show_on_map = (TextView) v.findViewById(R.id.card_show_on_map);
        }



    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<searchResultPOJO> searchResultData) {
        this.searchResultData = searchResultData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards, parent, false);
        // set the view's size, margins, paddings and layout parameters
       /* v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(v.getContext(), "Clicked", Toast.LENGTH_LONG).show();
               // v.getContext().startActivity(new Intent(v.getContext(),showOnMap.class));
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=12.961598700000000000,77.594562699999980000&daddr=12.971598700000000000,77.594462699999980000"));
                v.getContext().startActivity(intent);
            }
        });*/
        ViewHolder vh = new ViewHolder(v);
       /* vh.card_show_on_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=12.961598700000000000,77.594562699999980000&daddr=12.971598700000000000,77.594462699999980000"));
                v.getContext().startActivity(intent);
            }
        });*/

        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.card_biz_name.setText(searchResultData.get(position).getBusinessName());
        holder.card_distance.setText(searchResultData.get(position).getBusinessDistance());
        holder.card_contact.setText(searchResultData.get(position).getBusinessContact());
        holder.card_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + holder.card_contact.getText().toString()));
                v.getContext().startActivity(callIntent);
            }
        });
        holder.card_show_on_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=12.961598700000000000,77.594562699999980000&daddr=12.971598700000000000,77.594462699999980000"));
                v.getContext().startActivity(intent);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return searchResultData.size();
    }
}
