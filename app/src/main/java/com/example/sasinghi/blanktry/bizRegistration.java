package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sasinghi on 3/28/2015.
 */
public class bizRegistration extends Activity {

    String response = null;
    JSONObject addThisBusiness;
    ServiceHandler myServiceHandler;
    MaterialDialog loading;
    Context context;
    CheckBox tagsChecked;
    EditText otherTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biz_registration);
        context = this;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

    }

    public void onBizRegisterClicked(View v){

        new callService().execute();


    }

    class callService extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

            loading = new MaterialDialog.Builder(bizRegistration.this)
                    .title(R.string.progress_dialog)
                    .content("Logging you in.")
                    .progress(true, 0)
                    .show();

        }
        @Override
        protected String doInBackground(String... params) {
            Intent i = getIntent();
            ArrayList<String> collectedInfo = i.getStringArrayListExtra("collectedInfo");
            String tags = "";
            tagsChecked = (CheckBox) findViewById(R.id.tailor);
            if( tagsChecked.isChecked()){
                tags += "Tailor,";
            }
            tagsChecked = (CheckBox) findViewById(R.id.painter);
            if( tagsChecked.isChecked()){
                tags += "Painter,";
            }
            tagsChecked = (CheckBox) findViewById(R.id.plumber);
            if( tagsChecked.isChecked()){
                tags += "Plumber,";
            }
            tagsChecked = (CheckBox) findViewById(R.id.carpenter);
            if( tagsChecked.isChecked()){
                tags += "Carpenter,";
            }
            tagsChecked = (CheckBox) findViewById(R.id.maid);
            if( tagsChecked.isChecked()){
                tags += "Maid,";
            }

            otherTags = (EditText) findViewById(R.id.otherTags);
            if(!otherTags.getText().toString().isEmpty()){
                tags += otherTags.getText().toString();
            }

            // remove
            // Store collectedInfo and tags in biz DB

            ServiceHandler myServiceHandler = new ServiceHandler();
            JSONObject addThisBusiness = new JSONObject();
            ArrayList<NameValuePair> addThisBusinessList = new ArrayList<NameValuePair>();
            try {
                addThisBusiness.put("name", collectedInfo.get(0).toString());
                addThisBusiness.put("street", collectedInfo.get(1).toString());
                addThisBusiness.put("landmark", collectedInfo.get(2).toString());
                addThisBusiness.put("country", collectedInfo.get(3).toString());
                addThisBusiness.put("email", collectedInfo.get(4).toString());
                addThisBusiness.put("pinCode", collectedInfo.get(5).toString());
                addThisBusiness.put("userName", collectedInfo.get(6).toString());
                addThisBusiness.put("password", collectedInfo.get(7).toString());
            /*addThisBusiness.put("rePassword", collectedInfo.get(8).toString());*/
                addThisBusiness.put("phone", collectedInfo.get(9).toString());
                addThisBusiness.put("category", tags);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            myServiceHandler = new ServiceHandler();
            response = myServiceHandler.makeServiceCall("addASerProv",2,addThisBusiness);

            return null;
        }
        protected void onPostExecute(String ab) {
            if (response != null) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Registered successfully! Login to continue.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(bizRegistration.this, appHome.class));
            } else {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong. Try later.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(bizRegistration.this, appHome.class));
            }
        }
    }
}
