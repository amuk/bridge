package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sasinghi on 3/25/2015.
 */
public class appHome extends Activity {

     private Button register;
    String response = null;
    String userId, type;
    JSONObject searchThis;
    ServiceHandler myServiceHandler;
    MaterialDialog loading;
    Context context;
    EditText userName,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        context = this;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
       /* register = (Button) findViewById(R.id.registerButton);
        register.setOnClickListener(this);*/

    }

    public void onRegisterClick(View v) {
        Intent i = new Intent(appHome.this, registration.class);
        startActivity(i);
    }

    public void onLoginClick(View v) {
        new MaterialDialog.Builder(this)
                .customView(R.layout.login_dialog, true)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .negativeColor(R.color.accent_material_light)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                         userName = (EditText) dialog.findViewById(R.id.user);
                         password = (EditText) dialog.findViewById(R.id.pass);
                        /*Intent i = new Intent(appHome.this, customerBase.class);
                        i.putExtra("userId", userId);
                        startActivity(i);*/

                        if (userName != null && password != null) {
                            //Toast.makeText(getApplicationContext(), "User:" + userName.getText().toString() + " Pass:" + password.getText().toString(), Toast.LENGTH_SHORT).show();
                            // Check credentials. Return if user is biz or customer. False if wrong credentials.
                            myServiceHandler = new ServiceHandler();

                            new callService().execute();

                        } else {
                            Toast.makeText(getApplicationContext(), "Incorrect credentials. Try again.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }

    class callService extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

            loading = new MaterialDialog.Builder(context)
                    .title(R.string.progress_dialog)
                    .content("Logging you in.")
                    .progress(true, 0)
                    .show();

        }

        @Override
        protected String doInBackground(String... params) {
            searchThis = new JSONObject();
            try {
                searchThis.put("userName", userName.getText().toString());
                searchThis.put("password", password.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            response = myServiceHandler.makeServiceCall("checkCred", 2, searchThis);
            return null;
        }

        protected void onPostExecute(String ab) {
            System.out.println(response);
            if (response != null) {
                loading.dismiss();
                JSONObject returnedJson;
                String type = "";
                String userId = "";
                try {
                    System.out.println(response);
                    returnedJson = new JSONObject(response);
                    userId = returnedJson.get("id").toString();
                    type = returnedJson.get("type").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (type.equalsIgnoreCase("customer")) {
                    Intent i = new Intent(appHome.this, customerBase.class);
                    i.putExtra("userId", userId);
                    startActivity(i);
                } else if (type.equalsIgnoreCase("business")) {
                    Intent i = new Intent(appHome.this, businessHome.class);
                    i.putExtra("userId", userId);
                    startActivity(i);
                }
            }else{
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Login attempt failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


