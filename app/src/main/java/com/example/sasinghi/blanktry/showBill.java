package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by sasinghi on 4/9/2015.
 */
public class showBill extends Activity {

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_bill);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        context=this;

        /*TableLayout ll = (TableLayout) findViewById(R.id.billTable);
        android.widget.TableRow.LayoutParams p = new android.widget.TableRow.LayoutParams(800,TableLayout.LayoutParams.WRAP_CONTENT,1.0f);
        int leftMargin=50;
        int topMargin=10;
        int rightMargin=70;
        int bottomMargin=10;
        p.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        for(int pos=0;pos<13;pos++) {
            TableRow row = new TableRow(context);
            row.setPadding(0, 0, 0, 0);
           // row.setBackgroundColor(getResources().getColor(R.color.rateList));
            row.setGravity(3);
            TableLayout.LayoutParams tableRowParams =
                    new TableLayout.LayoutParams
                            (0, TableLayout.LayoutParams.WRAP_CONTENT, 1.0f);
            tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            row.setLayoutParams(p);
            TextView serviceName = new TextView(context);
            TextView serviceRate = new TextView(context);
            serviceName.setText("serviceName");
            //serviceName.setTextSize(24f);
            serviceName.setLayoutParams(p);
            serviceRate.setGravity(5);
            serviceRate.setText("50");
           // serviceRate.setTextSize(30f);
            serviceRate.setLayoutParams(p);

            row.addView(serviceName);
            row.addView(serviceRate);
            ll.addView(row, pos);
        }
*/
        View v = new DrawView(this);
        //v.setBackgroundColor(Color.WHITE);
        //ll.addView(v);


        }
    class DrawView extends View {
        Paint paint = new Paint();

        public DrawView(Context context) {
            super(context);
            paint.setColor(Color.WHITE);
        }
        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine(10, 10, 90, 10, paint);
            canvas.drawLine(10, 20, 90, 20, paint);

        }
    }

}
