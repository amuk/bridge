package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by sasinghi on 3/29/2015.
 */
public class searchResult extends Activity {

    JSONParser jsonparser = new JSONParser();
    String myDataset =null;
    JSONObject jobj = null;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    List<searchResultPOJO> searchList=new ArrayList<searchResultPOJO>();
    Context context;
    MaterialDialog loading;
    private String proximity;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results);
        Intent i = getIntent();
        proximity = i.getStringExtra("proximity");
        category = i.getStringExtra("category");
        SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.search_swipe_refresh_layout) ;
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });
        context = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        new retrievedata().execute();

        // specify an adapter (see also next example)
        searchResultPOJO dummy = new searchResultPOJO("Loading..","","");
        searchList = new ArrayList<searchResultPOJO>();
        searchList.add(dummy);
        mAdapter = new MyAdapter(searchList);
        mRecyclerView.setAdapter(mAdapter);
    }

    void refreshItems() {
        // Load items
        // ...

        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.search_swipe_refresh_layout);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    class retrievedata extends AsyncTask<String,String,String>{

        protected void onPreExecute(){

           loading= new MaterialDialog.Builder(context)
                    .title(R.string.progress_dialog)
                    .content(R.string.please_wait)
                    .progress(true, 0)
                    .show();

        }

        @Override
        protected String doInBackground(String... arg0) {
            ServiceHandler myServiceHandler = new ServiceHandler();
            JSONObject searchThis = new JSONObject();
            try {
                searchThis.put("radius", proximity);
                searchThis.put("category", category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
           // String response = myServiceHandler.makeServiceCall("http://ip/Ihack/rest/IHackService/servicename",1,searchThis);
            // add the response in searchList
            // Hardcoded search results
            searchList=null;
            searchList = new ArrayList<searchResultPOJO>();
            searchResultPOJO dummy = new searchResultPOJO("Raja Ram","+919743106982","3 kms");
            searchList.add(dummy);
            dummy = new searchResultPOJO("Sita Ram","+919019552528","4.3 kms");
            searchList.add(dummy);
            dummy = new searchResultPOJO("Mahalakshmi","+919019552528","4.7 kms");
            searchList.add(dummy);
            dummy = new searchResultPOJO("Ramu","+919019552528","5 kms");
            searchList.add(dummy);
            dummy = new searchResultPOJO("Rajesh","+919019552528","5 kms");
            searchList.add(dummy);
            // End of hardcoded data
            return null;
        }

        protected void onPostExecute(String ab){
            System.out.println(searchList.size()+ " is the size of result set");
            mAdapter.notifyDataSetChanged();
            mAdapter = new MyAdapter(searchList);
            mRecyclerView.setAdapter(mAdapter);
            loading.dismiss();
        }

    }

}
