package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sasinghi on 3/28/2015.
 */
public class businessHome extends Activity {

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    protected LocationManager locationManager;
    Location location;
    JSONObject updateLocation;
    ServiceHandler myServiceHandler;
    MaterialDialog loading;
    String response;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_home);
        TextView menu = (TextView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onMenuClicked(v);
            }
        });
        TextView bill = (TextView) findViewById(R.id.bill);
        bill.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onNewBillClicked(v);
            }
        });
        TextView analysis = (TextView) findViewById(R.id.analysis);
        analysis.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onViewSalesClicked(v);
            }
        });
        TextView bizlocation = (TextView) findViewById(R.id.bizLocationUpdate);
        bizlocation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onLocationPollClicked(v);
            }
        });

    }

    public void onMenuClicked(View v){
        Intent i = new Intent(businessHome.this, rateCard.class);
        i.putExtra("userId",getIntent().getStringExtra("userId"));
        startActivity(i);
    }

    public void onLocationPollClicked(View v){
        /*Intent i = new Intent(businessHome.this, locationPoll.class);
        startActivity(i);*/
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,new MyLocationListener());
        showCurrentLocation();


    }

    protected void showCurrentLocation() {


        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


        if (location != null) {

            String message = String.format(

                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",

                    location.getLongitude(), location.getLatitude()

            );
            Toast.makeText(businessHome.this, message, Toast.LENGTH_LONG).show();
            // Update in DB
            ServiceHandler myServiceHandler = new ServiceHandler();
            updateLocation = new JSONObject();
            try {
                updateLocation.put("id", getIntent().getStringExtra("userId"));
                updateLocation.put("lat", String.format("", location.getLatitude()));
                updateLocation.put("long", String.format("", location.getLongitude()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new callService().execute();

        }

    }

    class callService extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

            loading = new MaterialDialog.Builder(businessHome.this)
                    .title(R.string.progress_dialog)
                    .content("Updating location.")
                    .progress(true, 0)
                    .show();

        }
        @Override
        protected String doInBackground(String... params) {
            myServiceHandler = new ServiceHandler();
            response = myServiceHandler.makeServiceCall("updateLoc",2,updateLocation);
            if (response != null) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Updated successfully.", Toast.LENGTH_SHORT).show();
            } else {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Location can't be updated at this moment.", Toast.LENGTH_SHORT).show();
            }
                return null;
        }
        protected void onPostExecute(String ab) {


            }
        }


    public void onViewSalesClicked(View v){
        Intent i = new Intent(businessHome.this, showAnalysis.class);
        i.putExtra("userId",getIntent().getStringExtra("userId"));
        startActivity(i);
    }

    public void onNewBillClicked(View v){
        Intent i = new Intent(businessHome.this, createBill.class);
        i.putExtra("userId",getIntent().getStringExtra("userId"));
        startActivity(i);
    }

    public void onAddNotif(View v){
        Toast.makeText(businessHome.this, "Added a notif from this location", Toast.LENGTH_LONG).show();

    }

    private class MyLocationListener implements LocationListener {


        public void onLocationChanged(Location location) {

            String message = String.format(

                    "New Location \n Longitude: %1$s \n Latitude: %2$s",

                    location.getLongitude(), location.getLatitude()

            );

            Toast.makeText(businessHome.this, message, Toast.LENGTH_LONG).show();

        }


        public void onStatusChanged(String s, int i, Bundle b) {

            Toast.makeText(businessHome.this, "Provider status changed",

                    Toast.LENGTH_LONG).show();

        }


        public void onProviderDisabled(String s) {

            Toast.makeText(businessHome.this,

                    "Provider disabled by the user. GPS turned off",

                    Toast.LENGTH_LONG).show();

        }


        public void onProviderEnabled(String s) {

            Toast.makeText(businessHome.this,

                    "Provider enabled by the user. GPS turned on",

                    Toast.LENGTH_LONG).show();

        }


    }
}