package com.example.sasinghi.blanktry;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by sasinghi on 4/11/2015.
 */
public class customerBase extends Activity {

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    protected LocationManager locationManager;
    Location location;
    JSONObject sendToService;
    ServiceHandler myServiceHandler;
    EditText radius;
    String response;
    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    String userId;
    MaterialDialog loading;

    Timer myTimer = new Timer();

    class MyTimerTask extends TimerTask {
        public void run() {
            showNotification();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_base);
        userId = getIntent().getStringExtra("userId");
    }

    public void onAddAccountClicked(View v) {
        /*Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);*/
    }

    public void onSearchClicked(View v) {
        Intent i = new Intent(customerBase.this, customerHome.class);
        i.putExtra("userId", userId);
        startActivity(i);
    }

    public void onNotificationStart(View v) {
        System.out.println("start notif");
        new MaterialDialog.Builder(this)
                .customView(R.layout.get_radius, true)
                .positiveText("Get Notification")
                .negativeText("Cancel")
                .negativeColor(R.color.accent_material_light)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        radius = (EditText) dialog.findViewById(R.id.radius);
                        //new callService().execute();
                    }
                })
               .show();




        MyTimerTask myTask = new MyTimerTask();
        myTimer = new Timer();
        myTimer.schedule(myTask, 3000, 15000);
        System.out.println("stop");
    }

    public void onNotificationStop(View v) {
        System.out.println("Stopping");
        cancelNotification(0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void cancelNotification(int notificationId) {

        System.out.println("Stopping inside cancelNotification");
        if (Context.NOTIFICATION_SERVICE != null && myTimer != null) {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
            nMgr.cancel(notificationId);
            myTimer.cancel();
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void showNotification() {

        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

// intent triggered, you can add other intent for other actions
        Intent intent = new Intent(customerBase.this, navigate.class);
        PendingIntent pIntent = PendingIntent.getActivity(customerBase.this, 0, intent, 0);

// this is it, we'll build the notification!
// in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = new Notification.Builder(this)

                .setContentTitle("A new offer awaits!")
                .setContentText("Free goodies")
                .setSmallIcon(R.drawable.add)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .addAction(R.drawable.add, "Navigate", pIntent)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

// If you want to hide the notification after it was selected, do the code below
// myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);

    }

    class callService extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

            Toast.makeText(getApplicationContext(), "Getting notifications", Toast.LENGTH_SHORT).show();


        }
        @Override
        protected String doInBackground(String... params) {
            sendToService = new JSONObject();

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES,
                    MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());

            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


            if (location != null) {

                String message = String.format(

                        "Current Location \n Longitude: %1$s \n Latitude: %2$s",

                        location.getLongitude(), location.getLatitude()

                );
            }
                try {
                    sendToService.put("radius", radius.getText().toString());
                    sendToService.put("lat", String.format("" + location.getLatitude()));
                    sendToService.put("long", String.format(""+location.getLongitude()));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                myServiceHandler = new ServiceHandler();
                response = myServiceHandler.makeServiceCall("notifs",2,sendToService);



            return null;

       }
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        protected void onPostExecute(String ab) {
            if (response != null) {

            Notification mNotification = new Notification.Builder(customerBase.this)

                    .setContentTitle("A new offer!")
                    .setContentText("Great Job !")
                    .setSmallIcon(R.drawable.add)
                    .setSound(soundUri)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            // If you want to hide the notification after it was selected, do the code below
            // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(0, mNotification);



        } else {

            Toast.makeText(getApplicationContext(), "Location can't be updated at this moment.", Toast.LENGTH_SHORT).show();
        }
    }


    private class MyLocationListener implements LocationListener {


        public void onLocationChanged(Location location) {

            String message = String.format(

                    "New Location \n Longitude: %1$s \n Latitude: %2$s",

                    location.getLongitude(), location.getLatitude()

            );

            Toast.makeText(customerBase.this, message, Toast.LENGTH_LONG).show();

        }


        public void onStatusChanged(String s, int i, Bundle b) {

            Toast.makeText(customerBase.this, "Provider status changed",

                    Toast.LENGTH_LONG).show();

        }


        public void onProviderDisabled(String s) {

            Toast.makeText(customerBase.this,

                    "Provider disabled by the user. GPS turned off",

                    Toast.LENGTH_LONG).show();

        }


        public void onProviderEnabled(String s) {

            Toast.makeText(customerBase.this,

                    "Provider enabled by the user. GPS turned on",

                    Toast.LENGTH_LONG).show();

        }


    }
}}

