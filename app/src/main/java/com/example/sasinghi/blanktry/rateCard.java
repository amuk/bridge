package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ObservableScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sasinghi on 4/9/2015.
 */
public class rateCard extends Activity {

    Context context;
    Location location;
    JSONObject getRateCard;
    ServiceHandler myServiceHandler;
    MaterialDialog loading;
    String response=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_rate_card);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        context=this;
       // new callService().execute();

       /* try {
            JSONArray testArray = new JSONArray("[{\"firstName\":\"John\", \"lastName\":\"Doe\"},{\"firstName\":\"Anna\", \"lastName\":\"Smith\"},{\"firstName\":\"Peter\",\"lastName\": \"Jones\"}]");


        System.out.println("------------------------");
        System.out.println(testArray.length());
        System.out.println(testArray.get(0).toString());
        System.out.println(testArray.getJSONObject(0).get("firstName"));
        System.out.println("------------------------");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        TableLayout ll = (TableLayout) findViewById(R.id.ratesTable);
        ObservableScrollView listView = (ObservableScrollView) findViewById(R.id.scroll_view);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToScrollView(listView);
        //ll.setLayoutParams(new TableLayout.LayoutParams(400,400,1.0f));
        *//*ll.setColumnStretchable(1,true);*//*
        System.out.println(ll.getChildCount());
                *//*int pos = ll.getChildCount()-1;*//*
        android.widget.TableRow.LayoutParams p = new android.widget.TableRow.LayoutParams(600,100,1.0f);
        //p.rightMargin = DisplayHelper.dpToPixel(10, context); // right-margin = 10dp
        int leftMargin=10;
        int topMargin=10;
        int rightMargin=10;
        int bottomMargin=10;
        p.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);


        for(int pos=0;pos<19;pos++) {
            TableRow row = new TableRow(context);
            row.setPadding(30, Integer.parseInt("20"), 30, 0);
            row.setGravity(3);
            TableLayout.LayoutParams tableRowParams=
                    new TableLayout.LayoutParams
                            (0,TableLayout.LayoutParams.WRAP_CONTENT,1.0f);
            tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            row.setLayoutParams(tableRowParams);
            *//*TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);

            *//*

            row.setBackgroundColor(getResources().getColor(R.color.rateList));
            TextView addName = new TextView(context);
            TextView addRate = new TextView(context);

            addName.setText("serviceName");
            //addName.setBackgroundColor(getResources().getColor(R.color.background_floating_material_dark));
            addName.setTextSize(24f);
            addName.setLayoutParams(p);
            // addName.setHintTextColor(getResources().getColor(R.color.accent));
            addRate.setText("50");
            addRate.setTextSize(30f);
           // addRate.setBackgroundColor(getResources().getColor(R.color.accent_material_light));
            addRate.setLayoutParams(p);
            //addRate.setHintTextColor(getResources().getColor(R.color.accent));
            row.addView(addName);
            row.addView(addRate);
            ll.addView(row, pos);
        }*/
    }

    public void onEditRateClicked(View v){
        Intent i = new Intent(rateCard.this, editRateCard.class);
        i.putExtra("user",getIntent().getStringExtra("user"));
        startActivity(i);
    }

    class callService extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

            loading = new MaterialDialog.Builder(rateCard.this)
                    .title(R.string.progress_dialog)
                    .content("Updating location.")
                    .progress(true, 0)
                    .show();

        }

        @Override
        protected String doInBackground(String... params) {
            getRateCard = new JSONObject();
            myServiceHandler = new ServiceHandler();
            try {
               // getRateCard.put("id",rateCard.this.getIntent().getStringExtra("userId"));
                getRateCard.put("id","dummyId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            response = myServiceHandler.makeServiceCall("addASerProv", 2, getRateCard);
            return null;
        }

        protected void onPostExecute(String ab) {
            System.out.println(response);
            try {
                JSONArray jsonResponse = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (response != null) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Updated successfully.", Toast.LENGTH_SHORT).show();
                TableLayout ll = (TableLayout) findViewById(R.id.ratesTable);
                ObservableScrollView listView = (ObservableScrollView) findViewById(R.id.scroll_view);
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.attachToScrollView(listView);
                //ll.setLayoutParams(new TableLayout.LayoutParams(400,400,1.0f));
        /*ll.setColumnStretchable(1,true);*/
                //System.out.println(ll.getChildCount());
                /*int pos = ll.getChildCount()-1;*/
                android.widget.TableRow.LayoutParams p = new android.widget.TableRow.LayoutParams(600, 100, 1.0f);
                //p.rightMargin = DisplayHelper.dpToPixel(10, context); // right-margin = 10dp
                int leftMargin = 10;
                int topMargin = 10;
                int rightMargin = 10;
                int bottomMargin = 10;
                p.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
                JSONArray returnedArray = null;

                try {
                    returnedArray = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(returnedArray.length() > 0){
                    for (int pos = 0; pos < returnedArray.length(); pos++) {
                        TableRow row = new TableRow(context);
                        row.setPadding(30, Integer.parseInt("20"), 30, 0);
                        row.setGravity(3);
                        TableLayout.LayoutParams tableRowParams =
                                new TableLayout.LayoutParams
                                        (0, TableLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                        tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
                        row.setLayoutParams(tableRowParams);
                        row.setBackgroundColor(getResources().getColor(R.color.rateList));
                        TextView addName = new TextView(context);
                        TextView addRate = new TextView(context);

                        addName.setText("serviceName");
                        //addName.setBackgroundColor(getResources().getColor(R.color.background_floating_material_dark));
                        addName.setTextSize(24f);
                        addName.setLayoutParams(p);
                        // addName.setHintTextColor(getResources().getColor(R.color.accent));
                        addRate.setText("50");
                        addRate.setTextSize(30f);
                        // addRate.setBackgroundColor(getResources().getColor(R.color.accent_material_light));
                        addRate.setLayoutParams(p);
                        //addRate.setHintTextColor(getResources().getColor(R.color.accent));
                        row.addView(addName);
                        row.addView(addRate);
                        ll.addView(row, pos);
                    }
                }
            else {

                    loading.dismiss();
                    Toast.makeText(getApplicationContext(), "You will need to add a service first.", Toast.LENGTH_SHORT).show();
                    new MaterialDialog.Builder(rateCard.this)
                            .customView(R.layout.add_to_rate_card, true)
                            .positiveText(R.string.Add)
                            .negativeText(R.string.Cancel)
                            .negativeColor(R.color.accent_material_light)
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    Toast.makeText(getApplicationContext(), "Add the new service", Toast.LENGTH_SHORT).show();
                                    TextView serviceName = (TextView) dialog.findViewById(R.id.service);
                                    TextView serviceRate = (TextView) dialog.findViewById(R.id.rate);
                                    getRateCard = new JSONObject();
                                    try {
                                        getRateCard.put("id", rateCard.this.getIntent().getStringExtra("userId"));
                                        getRateCard.put("serviceRate", serviceName.getText().toString());
                                        getRateCard.put("serviceName", serviceRate.getText().toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    myServiceHandler = new ServiceHandler();
                                    response = myServiceHandler.makeServiceCall("addASerProv", 2, getRateCard);
                                    if (response == null) {
                                        loading.dismiss();
                                        Toast.makeText(getApplicationContext(), "Oops, there was a problem when adding!", Toast.LENGTH_SHORT).show();
                                    } else {

                                        Toast.makeText(getApplicationContext(), "Successfully added!", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(rateCard.this, businessHome.class);
                                        i.putExtra("userId", rateCard.this.getIntent().getStringExtra("userId"));
                                        startActivity(i);

                                    }


                                }
                            })
                            .show();
                }

            }
            else
                Toast.makeText(getApplicationContext(), "An unexpected error occurred.", Toast.LENGTH_SHORT).show();

        }
    }
}

