package com.example.sasinghi.blanktry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by sasinghi on 3/30/2015.
 */
public class createBill extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_bill);

    }
    public void onGenerateBillClicked(View v){
        //Toast.makeText(getApplicationContext(), "Bill is generated and stored.", Toast.LENGTH_LONG).show();
        Intent i = new Intent(createBill.this, showBill.class);
        i.putExtra("user",getIntent().getStringExtra("user"));
        startActivity(i);
    }

}


